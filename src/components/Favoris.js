import React, { useState } from "react";

const Favoris = (props) => {
  return (
    <div className="container">


      {props.favoris.length > 0 ? (
        <div>
            <h5>Mes favoris : </h5>
          {props.favoris.map((f, index) => (
              <ul>
                  <li key={index}>{f}</li>
              </ul>
          ))}
        </div>
      ) : (
        null
      )}
    </div>
  );
};

export default Favoris;
