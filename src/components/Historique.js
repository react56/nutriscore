import React from "react";

const Historique = (props) => {
  return (
    <div className="container">
      {props.historique.length > 0 ? (
        <div>
          <h5>Mon historique de recherche</h5>
          {props.historique.map((h, index) => (
            <ul>
              <li key={index}>{h}</li>
            </ul>
          ))}
        </div>
      ) : null}
    </div>
  );
};

export default Historique;
