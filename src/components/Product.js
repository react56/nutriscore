import  {React, useState} from "react";
import {dejaFavoris} from '../services/DataService'

export const Product = (props) => {
  const [isFavoris, setIsFavoris] = useState(dejaFavoris(props.product != undefined ? props.product.id : false))
  const ajouterFavoris = (id) => {
    setIsFavoris(true)
    props.ajouterFavoris(id)

  }

  const retirerFavoris = (id) => {
    setIsFavoris(false)
    props.retirerFavoris(id)
}

  return (
    <div>
      {props.product != undefined ? (
        <div className="row">
          <div className="container">
            <h1 className="my-4">{props.product.product_name_fr}</h1>

            <div className="row">
              <div className="col-md-4">
                <img
                  className="img-fluid"
                  src={props.product.image}
                  alt=""
                ></img>
              </div>

              <div className="col-md-8">
                <div className="row">
                  {props.product.salt > 1 ? (
                    <div className="col">
                      <div className="col-md-4 block">
                        <div className="circle circle-red">
                          <p>Sel</p>
                        </div>
                      </div>
                      {props.product.salt} g
                    </div>
                  ) : (
                    <div className="col">
                      <div className="col-md-4 block">
                        <div className="circle circle-green">
                          <p>Sel</p>
                        </div>
                      </div>
                      {props.product.salt} g
                    </div>
                  )}

                  {props.product.sugar > 1 ? (
                    <div className="col">
                      <div className="col-md-4 block">
                        <div className="circle circle-red">
                          <p>Sucre</p>
                        </div>
                      </div>
                      {props.product.sugar} g
                    </div>
                  ) : (
                    <div className="col">
                      <div className="col-md-4 block">
                        <div className="circle circle-green">
                          <p>Sucre</p>
                        </div>
                      </div>
                      {props.product.sugar} g
                    </div>
                  )}

                  {props.product.fat > 1 ? (
                    <div className="col">
                      <div className="col-md-4 block">
                        <div className="circle circle-red">
                          <p>Gras</p>
                        </div>
                      </div>
                      {props.product.fat} g
                    </div>
                  ) : (
                    <div className="col">
                      <div className="col-md-4 block">
                        <div className="circle circle-green">
                          <p>Gras</p>
                        </div>
                      </div>
                      {props.product.fat} g
                    </div>
                  )}
                </div>

                <h3 className="my-3">Ingrédients</h3>
                <p>{props.product.ingredients_text}</p>
                <div className="row">
                <div className="col-md-6">
                <h3 className="my-3">Informations</h3>
                <ul>
                  <li>
                    empreinte carbone :{" "}
                    {
                      props.product
                        .carbon_footprint_percent_of_known_ingredients
                    }{" "}
                    g
                  </li>
                  <li>
                    Allergies : {props.product.allergens_from_ingredients}
                  </li>
                  <li>
                    Huile de palme :{" "}
                    {props.product.ingredients_from_palm_oil_n != 0 ? (
                      <svg
                        width="1em"
                        height="1em"
                        viewBox="0 0 16 16"
                        className="bi bi-emoji-frown"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"
                        />
                        <path
                          fill-rule="evenodd"
                          d="M4.285 12.433a.5.5 0 0 0 .683-.183A3.498 3.498 0 0 1 8 10.5c1.295 0 2.426.703 3.032 1.75a.5.5 0 0 0 .866-.5A4.498 4.498 0 0 0 8 9.5a4.5 4.5 0 0 0-3.898 2.25.5.5 0 0 0 .183.683z"
                        />
                        <path d="M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z" />
                      </svg>
                    ) : (
                      <svg
                        width="1em"
                        height="1em"
                        viewBox="0 0 16 16"
                        className="bi bi-emoji-smile"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"
                        />
                        <path
                          fill-rule="evenodd"
                          d="M4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683z"
                        />
                        <path d="M7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm4 0c0 .828-.448 1.5-1 1.5s-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5z" />
                      </svg>
                    )}
                  </li>
                </ul>
                </div>
                <div className="col-md-6">
                <img
                      className="imageNutriscore"
                      src={`https://static.openfoodfacts.org/images/misc/nutriscore-${props.product.nutriscore_grade}.svg`}
                    ></img>
                    </div>
                </div>
              </div>
              <div className="row m-1">
                                {isFavoris ?
                                    (
                                        <button onClick={() => {retirerFavoris(props.product.id)}} className="btn col btn-danger">Retirer des favoris</button>
                                    ) :
                                    (
                                        <button onClick={() => {ajouterFavoris(props.product.id)}} className="btn col btn-warning">Ajouter aux favoris</button>
                                    )
                                }
                </div>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
};
