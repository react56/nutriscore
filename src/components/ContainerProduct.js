import React, { Component } from 'react';
import { Input } from 'antd';
import 'antd/dist/antd.css';
import { get } from '../services/ApiService'
import { Product } from './Product';
const { Search } = Input;


class ContainerProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: undefined,
            loading: false
        }
    }

    searchProduct = (e) => {
        const code = e.target.value
        this.setState({
            loading: true
        })
        get(code).then(result => {
            let tmpProduct = {
                id: result.product.id,
                product_name_fr: result.product.product_name,
                image_front_small_url: result.product.image_front_small_url,
                ingredients_text: result.product.ingredients_text,
                carbon_footprint_percent_of_known_ingredients: result.product.carbon_footprint_percent_of_known_ingredients,
                ingredients_from_palm_oil_n: result.product.ingredients_from_palm_oil_n,
                nutriscore_grade: result.product.nutriscore_grade,
                allergens_from_ingredients: result.product.allergens_from_ingredients,
                product_quantity: result.product.product_quantity,
                energy_value: result.product.energy_value,
                salt: result.product.nutriments.salt_100g,
                sugar: result.product.nutriments.sugars_100g,
                fat: result.product.nutriments.fat_100g,
                image: result.product.image_front_url
            }
            this.setState({
                product: tmpProduct,
                loading: false
            })
            this.props.ajouterHistorique(tmpProduct.id)
        }).catch(err => {
            alert('aucun produit avec ce code')
            this.setState({
                loading: false
            })
        })
    }

    render() {
        return (
            <div className="container">
                <p>Codes à tester : - 3270190155584 - 3017760290692 - 3564700004845</p>
                <div className="row justify-content-center">
                <div class="col-5"><Search onPressEnter={this.searchProduct} placeholder="Merci de saisir le code du produit" loading={this.state.loading} size="large" /></div>
                </div>
                <Product idProduct={this.props.idProduct} isFavoris={this.props.isFavoris} ajouterFavoris={this.props.ajouterFavoris} retirerFavoris={this.props.retirerFavoris} product={this.state.product}></Product>
            </div>
        );
    }
}

export default ContainerProduct;