const baseApi = "https://world.openfoodfacts.org/api/v0/product/"
export const get = (code) => {
    return fetch(`${baseApi}${code}.json`).then(res => res.json())
}
