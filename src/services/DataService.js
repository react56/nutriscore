export let favoris = []
export let historiques = []

export const ajouterAuFavoris = (id) => {
    if(!dejaFavoris(id)) {
        favoris.push(id)
    }
}

export const retirerDesFavoris = (id) => {
    favoris = favoris.filter(f => f != id)
}

export const dejaFavoris = (id) => {
    const fav = favoris.find(f => f == id)
    return fav != undefined
}


export const ajouterHistos = (id) => {
    if(!dejaHistos(id)) {
        historiques.push(id)
    }
}

export const dejaHistos = (id) => {
    const hist = historiques.find(h => h == id)
    return hist != undefined
}