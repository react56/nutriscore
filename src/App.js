import { useState } from 'react';
import './App.css';
import ContainerProduct from './components/ContainerProduct';
import { favoris, historiques, ajouterAuFavoris, retirerDesFavoris, ajouterHistos } from './services/DataService'
import Favoris from './components/Favoris'
import Historique from './components/Historique'

function App() {

  const [favs, setFavs] = useState(favoris)
  const [histos, setHistos] = useState(historiques)
  const [isFavoris , setIsFavoris] = useState(false)

  const ajouterFavoris = (id) => {
    ajouterAuFavoris(id)
    setFavs([...favoris])
    console.log(favoris)
  }

  const retirerFavoris = (id) => {
    retirerDesFavoris(id)
    setFavs([...favoris])
}

const ajouterHistorique = (id) => {
  ajouterHistos(id)
  setHistos([...historiques])
}

  return (
    <>
    <ContainerProduct isFavoris={isFavoris} ajouterHistorique={ajouterHistorique} ajouterFavoris={ajouterFavoris} retirerFavoris={retirerFavoris}></ContainerProduct>
    <Favoris favoris={favs}></Favoris>
    <Historique historique={histos}></Historique>
    </>
  );
}

export default App;
